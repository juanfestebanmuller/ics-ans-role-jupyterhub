# ics-ans-role-jupyterhub

Ansible role to install jupyterhub.

## Role Variables

Note that boolean variables should be entered as boolean (`true`/`false`),
not as strings (`"True"`/`"False"`).

```yaml
---
jupyterhub_tag: 2.3.1
jupyterhub_docker_container_image: registry.esss.lu.se/ics-docker/notebook
jupyterhub_hub_docker_container_image: registry.esss.lu.se/ics-docker/jupyterhub
# List of "src:dest" volumes to be mounted in the jupyterhub container
# jupyterhub_extra_volumes:
#   - "/mnt/dir:/mnt/mydir"
#   - "/shares/users:/shares/users"
jupyterhub_extra_volumes: []
# Dict of src:dest volumes to be mounted in the notebook spawned container
# jupyterhub_notebook_volumes:
#   /mnt/dir: /mnt/mydir
#   /shares/users/{username}: /home/{username}"
jupyterhub_notebook_volumes: {}
jupyterhub_remove_containers: true
jupyterhub_network: jupyterhub-network
jupyterhub_dir: /opt/jupyterhub
jupyterhub_data: /opt/jupyterhub/data
jupyterhub_whitelist: []
jupyterhub_admin_users: []
jupyterhub_admin_access: false
jupyterhub_port: "8000"
jupyterhub_traefik_host: "{{ ansible_fqdn }}"
jupyterhub_traefik_frontend_rule: "Host:{{ jupyterhub_traefik_host }}"
jupyterhub_nfs_directories: []
jupyterhub_pull_notebook: false
jupyterhub_spawner_default_url: "/lab"
jupyterhub_epics_ca_auto_addr_list: "NO"
jupyterhub_epics_ca_addr_list: ""
jupyterhub_epics_pva_auto_addr_list: "NO"
jupyterhub_epics_pva_addr_list: ""
# Following variable can be defined to use
# authenticated API call when accessing GitLab
# jupyterhub_gitlab_access_token: secret_token

# JupyterHub Authenticator
# https://github.com/jupyterhub/jupyterhub/wiki/Authenticators
# ldapauthenticator | dummyauthenticator
jupyterhub_authenticator: ldapauthenticator
# Dummy Authenticator static password
jupyterhub_dummyauthenticator_password: "secret"
# LDAP config when using LDAP Authenticator
jupyterhub_ldap_server_address: "myldap.example.org"
jupyterhub_ldap_use_ssl: true
jupyterhub_ldap_bind_dn_template:
  - "uid={username},ou=Users,dc=esss,dc=lu,dc=se"
jupyterhub_ldap_lookup_dn: false
jupyterhub_ldap_lookup_dn_user_dn_attribute: cn
jupyterhub_ldap_use_lookup_dn_username: false
jupyterhub_ldap_search_filter: "(&(samAccountType=805306368)({login_attr}={login}))"
jupyterhub_ldap_user_search_base: "OU=ESS Users,DC=esss,DC=lu,DC=se"
jupyterhub_ldap_search_user: ldapuser
jupyterhub_ldap_search_password: secret
jupyterhub_ldap_user_attribute: "sAMAccountName"
jupyterhub_ldap_enable_auth_state: true
# hex-encoded 32-byte key for encryption of auth_state
# example: export JUPYTERHUB_CRYPT_KEY=$(openssl rand -hex 32)
jupyterhub_crypt_key: xxxxxx
jupyterhub_proxy: ""


```

The variable `jupyterhub_nfs_directories` can be used to mount directories via nfs:

```yaml
jupyterhub_nfs_directories:
  - path: /mnt/nfs/home
    src: myserver:/data/home
    opts: timeo=14,intr
```

The variable `jupyterhub_traefik_host` can be set to a single host or a list of hosts:

```yaml
jupyterhub_traefik_host: jupyterhub.domain.com,another.domain.com
```

If you don't want to use a list of Host but a HostRegexp for example, you can override the `jupyterhub_traefik_frontend_rule` variable.

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-jupyterhub
```

## License

BSD 2-clause
