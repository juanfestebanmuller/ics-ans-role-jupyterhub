import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ["MOLECULE_INVENTORY_FILE"]
).get_hosts("all")


def test_jupyterhub_containers(host):
    with host.sudo():
        cmd = host.run("docker ps")
    assert cmd.rc == 0
    # Get the names of the running containers
    # - skip the first line (header)
    # - take the last element of the remaining lines
    names = sorted([line.split()[-1] for line in cmd.stdout.strip().split("\n")[1:]])
    assert names == ["jupyterhub", "traefik_proxy"]


def test_jupyterhub_index(host):
    # This tests that traefik forwards traffic to jupyterhub
    # and that we can access the jupyterhub index page
    cmd = host.run("curl -H Host:jupyterhub.docker.localhost -k -L https://localhost")
    assert "<title>JupyterHub</title>" in cmd.stdout


def test_jupyterhub_volumes(host):
    with host.sudo():
        cmd = host.run("docker inspect jupyterhub | grep -A5 Binds")
    assert cmd.rc == 0
    assert "/var/run/docker.sock" in cmd.stdout
    assert "/opt/jupyterhub/data" in cmd.stdout
    assert "/opt/jupyterhub/jupyterhub_config.py" in cmd.stdout
    if (
        host.ansible.get_variables()["inventory_hostname"]
        == "ics-ans-role-jupyterhub-default"
    ):
        assert "/shares/users" in cmd.stdout
    else:
        assert "/shares/users" not in cmd.stdout


def test_jupyterhub_config(host):
    with host.sudo():
        config = host.file("/opt/jupyterhub/jupyterhub_config.py")
        if (
            host.ansible.get_variables()["inventory_hostname"]
            == "ics-ans-role-jupyterhub-default"
        ):
            assert config.contains(
                "c.JupyterHub.authenticator_class = LDAPAuthenticatorAuthState"
            )
            assert not config.contains("c.LDAPAuthenticator.lookup_dn_search_filter")
            assert not config.contains("c.Authenticator.whitelist")
            assert not config.contains("c.Authenticator.admin_users")
            assert config.contains(
                "c.DockerSpawner.volumes = {'/shares/controlroom': '/shares/controlroom', '/shares/scratch': '/shares/scratch', '/shares/users/{username}': '/shares/users/{username}'}"
            )
            assert config.contains("c.Spawner.pre_spawn_hook = create_user_home")
        elif (
            host.ansible.get_variables()["inventory_hostname"]
            == "ics-ans-role-jupyterhub-ad"
        ):
            assert config.contains("c.LDAPAuthenticator.lookup_dn_search_filter")
            assert config.contains(
                "c.JupyterHub.authenticator_class = LDAPAuthenticator"
            )
            assert not config.contains(
                "c.JupyterHub.authenticator_class = LDAPAuthenticatorAuthState"
            )
            assert config.contains("c.DockerSpawner.volumes = {}")
            assert not config.contains("c.Spawner.pre_spawn_hook")
        elif (
            host.ansible.get_variables()["inventory_hostname"]
            == "ics-ans-role-jupyterhub-dummy"
        ):
            assert config.contains("c.JupyterHub.authenticator_class = 'dummy'")
            assert config.contains(
                'c.Authenticator.whitelist = {"foo", "lcr-ws01", "lcr-ws02"}'
            )
            assert config.contains('c.Authenticator.admin_users = {"foo"}')
            assert config.contains("c.DockerSpawner.volumes = {}")
            assert not config.contains("c.Spawner.pre_spawn_hook")
